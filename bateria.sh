#!/bin/bash
#
# -*- ENCODING: UTF-8 -*-
# Este programa es software libre. Puede redistribuirlo y/o
# modificarlo bajo los términos de la Licencia Pública General
# de GNU según es publicada por la Free Software Foundation,
# bien de la versión 2 de dicha Licencia o bien (según su
# elección) de cualquier versión posterior.
#
# Si usted hace alguna modificación en esta aplicación,
# deberá siempre mencionar al autor original de la misma.
#
# DesdeLinux.net CC-BY-SA 2015
# Autor: ELAV <elav@desdelinux.net> <http://www.systeminside.net>

BATLVL=$(cat /sys/class/power_supply/BAT1/capacity)

if [ $BATLVL -ge 80 ]; then

    if [ ! -a /tmp/batwarn ]; then
        notify-send --urgency=critical --expire-time=5000 --app-name=Bateria --icon=battery "Notificación de Batería" "Desconecta la corriente eléctrica por favor" ;
        touch /tmp/batwarn ;
    elif [ $BATLVL -le 40 ]; then

        if [ ! -a /tmp/batwarn ]; then
            notify-send --urgency=critical --expire-time=5000 --app-name=Bateria --icon=battery "Notificación de Batería" "Conecta la corriente eléctrica por favor"
            touch /tmp/batwarn ;
        fi
    else
        if [ -a /tmp/batwarn ]; then
            rm -f /tmp/batwarn ;
        fi
   fi     
fi